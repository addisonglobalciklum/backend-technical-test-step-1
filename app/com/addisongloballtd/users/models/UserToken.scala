package com.addisongloballtd.users.models

import org.joda.time.DateTime

//It is the concatenation of the userId and the current time. For example: user123_2017-01-01T10:00:00.000
sealed trait UserToken {
}

final case class ValidUserToken(token: String) extends UserToken
final case object InvalidUserToken extends UserToken

object UserToken {
  def apply(user: User, date: DateTime): UserToken = {
    user match {
      case ValidUser(userId) => new ValidUserToken(userId.concat("_").concat(date.toString()))
      case InvalidUser       => InvalidUserToken
    }
  }
}
