package com.addisongloballtd.users.models

sealed trait User

final case class ValidUser(userId: String) extends User

final case object InvalidUser extends User

object User {
  def apply(credential: Credentials): User = {
    if(credential.isValid())
      ValidUser("123")
    else
      InvalidUser
  }
}
