package com.addisongloballtd.users.services.TokenService

//This provider to do mixing when other service have to use function from SyncTokenService without using library like Guice.
//It's more clean and the logic is encapsulated in a singleton object because It's only algebraic logic
trait SyncTokenServiceProvider {
  def syncTokenService : SyncTokenService = SyncTokenServiceImpl
}
