package com.addisongloballtd.users.services.TokenService

trait AsyncTokenServiceProvider {
  def asyncTokenService : AsyncTokenService = AsyncTokenServiceImpl
}

