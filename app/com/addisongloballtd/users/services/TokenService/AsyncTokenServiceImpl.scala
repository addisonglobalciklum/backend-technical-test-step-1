package com.addisongloballtd.users.services.TokenService

import com.addisongloballtd.users.models.{Credentials, User, UserToken}
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait AsyncTokenServiceImpl extends AsyncTokenService{

  protected def authenticate(credentials: Credentials): Future[User] = Future(User(credentials))

  protected def issueToken(user: User): Future[UserToken] = Future(UserToken(user, DateTime.now()))

  def requestToken(credentials: Credentials): Future[UserToken] = {
    for {
      user      <- authenticate(credentials)
      userToken <- issueToken(user)
    } yield userToken
  }

}
object AsyncTokenServiceImpl extends AsyncTokenServiceImpl