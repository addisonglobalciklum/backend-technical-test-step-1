package com.addisongloballtd.users.services.TokenService

import com.addisongloballtd.users.models.{Credentials, User, UserToken}

trait SyncTokenService {

  protected def authenticate(credentials: Credentials): User

  protected def issueToken(user: User): UserToken

  def requestToken(credentials: Credentials): UserToken

}
