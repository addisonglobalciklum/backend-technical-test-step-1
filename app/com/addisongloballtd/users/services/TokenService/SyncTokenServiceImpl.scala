package com.addisongloballtd.users.services.TokenService

import com.addisongloballtd.users.models.{Credentials, User, UserToken}
import org.joda.time.DateTime

trait SyncTokenServiceImpl extends SyncTokenService {

  protected def authenticate(credentials: Credentials): User = User(credentials)

  protected def issueToken(user: User): UserToken = UserToken(user, DateTime.now())

  def requestToken(credentials: Credentials): UserToken = issueToken(authenticate(credentials))

}
object SyncTokenServiceImpl extends SyncTokenServiceImpl
