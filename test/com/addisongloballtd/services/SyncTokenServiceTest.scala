package com.addisongloballtd.services

import com.addisongloballtd.users.models.{Credentials, User, ValidUserToken}
import com.addisongloballtd.users.services.TokenService.SyncTokenServiceImpl
import org.joda.time.{DateTime, DateTimeUtils}
import org.scalatest.{Matchers, WordSpec}
import org.scalatest._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._


class SyncTokenServiceTest extends PlaySpec {

  "SyncTokenServiceTest " should {
    "requestToken method: given a valid credentials response a ValidUser" in {
      //arrange
      DateTimeUtils.setCurrentMillisFixed(DateTime.now().getMillis)
      val time = DateTime.now()
      val credentials = Credentials("house", "HOUSE")
      val syncTokenServiceMock = new SyncTokenServiceImpl {}

      //act
      val result = syncTokenServiceMock.requestToken(credentials)

      DateTimeUtils.setCurrentMillisSystem()
      //assert
      assert(result === ValidUserToken("123_".concat(time.toString())))
    }
  }
}
