package com.addisongloballtd.services

import com.addisongloballtd.users.models.{Credentials, ValidUserToken}
import com.addisongloballtd.users.services.TokenService.AsyncTokenServiceImpl
import org.joda.time.{DateTime, DateTimeUtils}
import org.scalatestplus.play.PlaySpec
import scala.concurrent.ExecutionContext.Implicits.global

class AsyncTokenServiceTest extends PlaySpec {

  "AsyncTokenServiceTest " should {
    "requestToken method: given a valid credentials response a Future[UserToken]" in {
      //arrange
      DateTimeUtils.setCurrentMillisFixed(DateTime.now().getMillis)
      val time = DateTime.now()
      val credentials = Credentials("house", "HOUSE")
      val asyncTokenServiceMock = new AsyncTokenServiceImpl {}

      //act
      val resultF = asyncTokenServiceMock.requestToken(credentials)

      //assert
      DateTimeUtils.setCurrentMillisSystem()
      resultF.map{ result =>
        println(s"ingreso :$result")
        assert(result === ValidUserToken("1".concat(time.toString())))
      }.recover{
        case e => assert(false)
      }.map(x => x)

    }
  }

}
