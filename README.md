# Service Trait / Interface

Given these two synchronous and asynchronous definitions of the TokenService

```bash
trait SyncTokenService {
  protected def authenticate(credentials: Credentials): User
  protected def issueToken(user: User): UserToken

  def requestToken(credentials: Credentials): UserToken = ???
}
```

```bash
import scala.concurrent.Future

trait AsyncTokenService {
  protected def authenticate(credentials: Credentials): Future[User]
  protected def issueToken(user: User): Future[UserToken]

  def requestToken(credentials: Credentials): Future[UserToken] = ???
}
```

**Task**: Provide both implementations of requestToken in terms of authenticate and issueToken. By doing that, whoever implements the service will only need to implement authenticate and issueToken.

## Domain

The services are group in a domain called **users**

Inside **users** you could find:

- models
  These are objects that represent the entities related to bussiness domain

- services.TokenService
  There are a bussines logic that interact with models and controller
  - **Service** : This is a trait with the functionality's firms
  - **ServiceImpl** : This is bussiness logic. It's a trait and throuth an Object (Singleton pattern) you could use this bussiness logic
  - **ServiceProvider** : This the way to do mixing with other services without a library like Guice.

## Test

Run this using [sbt](http://www.scala-sbt.org/).  If you downloaded this project from <http://www.playframework.com/download> then you'll find a prepackaged version of sbt in the project directory:

```bash
sbt test
```